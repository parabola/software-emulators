#!/bin/bash

# UNRAR-Emulator, script to emulate nonfree unrar binary; run with bash, libarchive-tar and unar
# Copyright (C) 2015  Márcio Silva <coadde@parabola.nu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

unrar-emulator_usage() {
	echo 'UNRAR-Emulator v1.0 Free Software      Copyright (C) Márcio Silva'
	echo 'Script to emulate nonfree unrar binary; run with bash, libarchive-tar and unar'
	echo ''
	echo 'Usage: unrar <command> [-<switch1> -<switch2> ...] <archive> [...] <path_to_extract>'
	echo ''
	echo '<Commands>'
	echo '  e            Extract files without create a containing directory'
	echo '  l[b],v[b]    List archive contents [bare]'
	echo '  x            Extract files'
	echo ''
	echo '<Switches>'
	echo '  inul         Disable all messages'
	echo '  o[+|-]       Set the overwrite mode'
	echo '  or           Rename directories automatically'
	echo '  p[password]  Set password'
	exit 0
}

command="${1}"
parameters=("${@:2}")

for parameter in "${parameters[@]}"; do
	case "${parameter}" in
	-inul)
		verbose='no'
		unar_parameters+=('-q')
		;;
	-o+)
		unar_parameters+=('-f')
		;;
	-o-)
		tar_parameters+=('-k')
		unar_parameters+=('-s')
		;;
	-or)
		unar_parameters+=('-r')
		;;
	-p*)
		password="${parameter/-p/}"
		;;
	-*)
		echo "ERROR: Unknown option: ${parameter/-/}"
		exit 1
		;;
	*)
		if [ -z "${archive}" ]; then
			archive="${parameter}"
		elif [ -n "${archive}" ]; then
			directory="${parameter}"
		fi
		;;
	esac
done

if [ -z "${command}" ] || [ -z "${parameters}" ] || [ -z "${archive}" ]; then
	unrar-emulator_usage
fi

if [ "$command" == 'x' ]; then
	if [ -z "${password}" ]; then
		if [ -z "${directory}" ]; then
			unar "${unar_parameters[@]}" -k skip "${archive}"
		elif [ -n "${directory}" ]; then
			unar "${unar_parameters[@]}" -k skip -o "${directory}" "${archive}"
		fi
	elif [ -n "${password}" ]; then
		if [ -z "${directory}" ]; then
			unar "${unar_parameters[@]}" -k skip -p "${password}" "${archive}"
		elif [ -n "${directory}" ]; then
			unar "${unar_parameters[@]}" -k skip -p "${password}" -o "${directory}" "${archive}"
		fi
	fi
elif [ "${command}" == 'e' ]; then
	if [ -z "${password}" ]; then
		if [ -z "${directory}" ]; then
			unar -D "${unar_parameters[@]}" -k skip "${archive}"
		elif [ -n "${directory}" ]; then
			unar -D "${unar_parameters[@]}" -k skip -o "${directory}" "${archive}"
		fi
	elif [ -n "${password}" ]; then
		if [ -z "${directory}" ]; then
			unar -D "${unar_parameters[@]}" -k skip -p "${password}" "${archive}"
		elif [ -n "${directory}" ]; then
			unar -D "${unar_parameters[@]}" -k skip -p "${password}" -o "${directory}" "${archive}"
		fi
	fi
elif [ "${command}" == 'l' ] || [ "${command}" == 'lt' ] || [ "${command}" == 'lta' ] || [ "${command}" == 'v' ] || [ "${command}" == 'vt' ] || [ "${command}" == 'vta' ]; then
	if [ "${verbose}" != 'no' ]; then
		bsdtar tv "${tar_parameters[@]}" -f "${archive}"
	else
		exit 0
	fi
elif [ "${command}" == 'lb' ] || [ "${command}" == 'vb' ]; then
	if [ "${verbose}" != 'no' ]; then
		bsdtar t "${tar_parameters[@]}" -f "${archive}"
	else
		exit 0
	fi
else
	unrar-emulator_usage
fi
